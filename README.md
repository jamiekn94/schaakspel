# Multiplayer Chess #

This is a multiplayer chess web application.

**Features**

* Validation of movements for all the chess pieces
* Moving
* Removing of pieces when beaten

**Todo**

* Implementing check & checkmate
* Promoting pawn when reached opponents side
* Set that pawns are allowed to do two steps when in their begin position.
* Save removed pieces

**Technical**

* C#
* ASP.NET MVC 4
* Razor View Engine
* SignalR
* Bootstrap