﻿namespace ChessSignalR.Game.Models
{
    public class PositionModel
    {
        public int RowOldPosition { get; set; }
        public int RowNewPosition { get; set; }
        public int  ColumnOldPosition { get; set; }
        public int ColumnNewPosition { get; set; }
    }
}