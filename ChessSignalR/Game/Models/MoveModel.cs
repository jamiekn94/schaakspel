﻿namespace ChessSignalR.Game.Models
{
    public class MoveModel
    {
        /// <summary>
        /// Determine if the move is valid of the user
        /// </summary>
        public bool IsValidMove { get; set; }

        /// <summary>
        /// Delete the chess piece that is on this position from the opponent
        /// </summary>
        public string DeletePositionId { get; set; }
    }
}