﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChessSignalR.Game
{
    public enum ClientSide
    {
        White = 0, Black = 1
    }
    public class Client
    {
        public string ConnectionId { get; set; }
        public string Name { get; set; }
        public ClientSide Side { get; set; }
        /// <summary>
        /// PieceId - Position
        /// </summary>
        public Dictionary<string, string> AvailablePieces { get; set; }
        /// <summary>
        /// TODO: Add  deleted pieces into this dictionary
        /// </summary>
        public Dictionary<string, string> DeletedPieces { get; set; }

        public Client(string connectionId, string name, ClientSide clientSide)
        {
            // Set info
            ConnectionId = connectionId;
            Name = name;
            Side = clientSide;

            // Set pieces
            SetPieces(clientSide);
        }

        public bool HasPieceOnPosition(string position)
        {
            return AvailablePieces.Any(x => x.Value == position);
        }

        public void RemovePieceByPosition(string positionId)
        {
            AvailablePieces.Remove(AvailablePieces.First(x => x.Value == positionId).Key);
        }

        public void UpdatePiecePosition(string pieceId, string newPosition)
        {
            string key = AvailablePieces.First(x => x.Key == pieceId).Key;
            AvailablePieces[key] = newPosition;
        }

        public string GetPosition(string pieceId)
        {
            return AvailablePieces.FirstOrDefault(x => x.Key == pieceId).Value;
        }

        private void SetPieces(ClientSide clientSide)
        {
            if (clientSide == ClientSide.White)
            {
                AvailablePieces = new Dictionary<string, string>
                {
                    {"W1", "7A"},
                    {"W2", "7B"},
                    {"W3", "7C"},
                    {"W4", "7D"},
                    {"W5", "7E"},
                    {"W6", "7F"},
                    {"W7", "7G"},
                    {"W8", "7H"},
                    {"W9", "8A"},
                    {"W10", "8B"},
                    {"W11", "8C"},
                    {"W12", "8D"},
                    {"W13", "8E"},
                    {"W14", "8F"},
                    {"W15", "8G"},
                    {"W16", "8H"}
                };
            }
            else
            {
                AvailablePieces = new Dictionary<string, string>
                {
                    {"B1", "2A"},
                    {"B2", "2B"},
                    {"B3", "2C"},
                    {"B4", "2D"},
                    {"B5", "2E"},
                    {"B6", "2F"},
                    {"B7", "2G"},
                    {"B8", "2H"},
                    {"B9", "1A"},
                    {"B10", "1B"},
                    {"B11", "1C"},
                    {"B12", "1D"},
                    {"B13", "1E"},
                    {"B14", "1F"},
                    {"B15", "1G"},
                    {"B16", "1H"}
                };
            }
        }
    }
}