﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace ChessSignalR.Game
{
    public static class GameManager
    {
        private static Collection<Game> ActiveGames { get; set; }

        static GameManager()
        {
            ActiveGames = new Collection<Game>();
        }

        public static Game GetGame(string connectionId)
        {
            Game game = null;

            // Check if there are any users with this connectionId
            if (ActiveGames.Any(g => g.Users.Any(u => u.ConnectionId == connectionId)))
            {
                game = ActiveGames.FirstOrDefault(g => g.Users.FirstOrDefault(u => u.ConnectionId == connectionId).ConnectionId == connectionId);
            }

            return game;
        }

        public static Game GetGame(int gameId)
        {
            return ActiveGames.First(g=> g.GameId == gameId);
        }

        public static Game GetGameUserWaiting()
        {
            return ActiveGames.FirstOrDefault(g => g.Users.Count() == 1);
        }

        public static IEnumerable<Client> GetUsers(int gameId)
        {
            return ActiveGames.First(x => x.GameId == gameId).Users;
        }

        public static Client GetOtherUser(int gameId, string connectionId)
        {
            return GetUsers(gameId).FirstOrDefault(x => x.ConnectionId != connectionId);
        }

        public static void RemoveGame(Game game)
        {
            ActiveGames.Remove(game);
        }

        public static Client GetUser(string connectionId)
        {
            return
                ActiveGames.FirstOrDefault(
                    g => g.Users.FirstOrDefault(x => x.ConnectionId == connectionId).ConnectionId == connectionId)
                    .Users.First(x=> x.ConnectionId == connectionId);
        }

        public static void AddGame(int gameId, string connectionId, string name)
        {
            // Create new game
            var newGame = new Game(gameId, connectionId, ClientSide.White, name);

            // Add new game
            ActiveGames.Add(newGame);
        }
    }
}