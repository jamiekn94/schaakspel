﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using ChessSignalR.Game.Models;

namespace ChessSignalR.Game
{
    public enum ChessPiece
    {
        Loper, Toren, Paard, Pion, Koning, Koningin
    }

    public enum PositionLetter
    {
        A = 0, B = 1, C = 2, D = 3, E = 4, F = 5, G = 6, H = 7
    }

    public static class Pieces
    {
        #region Constructor
        static Pieces()
        {
            ChessPieces = new Dictionary<int, ChessPiece>();
            SetPieces();
        }
        #endregion

        #region Properties
        private static Dictionary<int, ChessPiece> ChessPieces { get; set; }
        #endregion

        #region Private Methods

        private static void SetPieces()
        {
            ChessPieces.Add(1, ChessPiece.Pion);
            ChessPieces.Add(2, ChessPiece.Pion);
            ChessPieces.Add(3, ChessPiece.Pion);
            ChessPieces.Add(4, ChessPiece.Pion);
            ChessPieces.Add(5, ChessPiece.Pion);
            ChessPieces.Add(6, ChessPiece.Pion);
            ChessPieces.Add(7, ChessPiece.Pion);
            ChessPieces.Add(8, ChessPiece.Pion);
            ChessPieces.Add(9, ChessPiece.Toren);
            ChessPieces.Add(10, ChessPiece.Paard);
            ChessPieces.Add(11, ChessPiece.Loper);
            ChessPieces.Add(12, ChessPiece.Koningin);
            ChessPieces.Add(13, ChessPiece.Koning);
            ChessPieces.Add(14, ChessPiece.Loper);
            ChessPieces.Add(15, ChessPiece.Paard);
            ChessPieces.Add(16, ChessPiece.Toren);
        }

        private static ChessPiece GetKindOfPiece(int id)
        {
            return ChessPieces.First(x => x.Key == id).Value;
        }

        public static MoveModel IsValidMove(string pieceId, string oldPosition, string newPosition, string connectionId, ClientSide clientSide)
        {
            int id = int.Parse(pieceId.Substring(1, pieceId.Length - 1));

            switch (GetKindOfPiece(id))
            {
                case ChessPiece.Pion:
                    {
                        return PionValidMove(oldPosition, newPosition, clientSide, connectionId, pieceId);
                    }
                case ChessPiece.Paard:
                {
                    return PaardValidMove(oldPosition, newPosition, connectionId, pieceId);
                }
                case ChessPiece.Loper:
                    {
                        return LoperValidMove(oldPosition, newPosition, connectionId, pieceId);
                    }
                case ChessPiece.Koningin:
                    {
                        return KoninginValidMove(oldPosition, newPosition, connectionId, pieceId);
                    }
                case ChessPiece.Koning:
                    {
                        return KoningValidMove(oldPosition, newPosition, connectionId, pieceId);
                    }
                case ChessPiece.Toren:
                    {
                        return TorenValidMove(oldPosition, newPosition, connectionId, pieceId);
                    }
                default:
                    {
                        throw new ArgumentOutOfRangeException("IsValidMove - Deze kind of piece bestaat niet.");
                    }
            }
            return null;
        }

        private static PositionLetter GetPositionLetter(string rowLetter)
        {
            rowLetter = rowLetter.ToUpper();

            switch (rowLetter)
            {
                case "A":
                    {
                        return PositionLetter.A;
                    }
                case "B":
                    {
                        return PositionLetter.B;
                    }
                case "C":
                    {
                        return PositionLetter.C;
                    }
                case "D":
                    {
                        return PositionLetter.D;
                    }
                case "E":
                    {
                        return PositionLetter.E;
                    }
                case "F":
                    {
                        return PositionLetter.F;
                    }
                case "G":
                    {
                        return PositionLetter.G;
                    }
                case "H":
                    {
                        return PositionLetter.H;
                    }
                default:
                    {
                        throw new ArgumentOutOfRangeException("De position letter moet tussen de a - h zijn");
                    }
            }
        }

        private static PositionModel GetPositions(string oldPosition, string newPosition)
        {
            // Get the letter of old position
            string oldColumnPositionLetter = oldPosition.Substring(1, 1).ToUpper();

            // Get the letter of the new position
            string newColumnPositionLetter = newPosition.Substring(1, 1).ToUpper();

            // Get the enum old position
            var enumColumnOldPosition = GetPositionLetter(oldColumnPositionLetter);

            // Get the enum new position
            var enumColumnNewPosition = GetPositionLetter(newColumnPositionLetter);

            // Get the column position of the old position
            int rowOldPosition = int.Parse(oldPosition.Substring(0, 1));

            // Get the column position of the new position
            int rowNewPosition = int.Parse(newPosition.Substring(0, 1));

            // Return position model
            return new PositionModel
            {
                ColumnNewPosition = (int)enumColumnNewPosition,
                ColumnOldPosition = (int)enumColumnOldPosition,
                RowNewPosition = rowNewPosition,
                RowOldPosition = rowOldPosition
            };
        }

        #endregion

        #region Validate Movements Methods

        #region Pion

        private static MoveModel PionValidMove(string oldPosition, string newPosition, ClientSide clientSide, string connectionId, string pieceId)
        {
            // Create return move class
            var moveModel = new MoveModel();

            // Get positions
            var positionModel = GetPositions(oldPosition, newPosition);

            // Get current game
            var activeGame = GameManager.GetGame(connectionId);

            // Get current user
            var currentUser = GameManager.GetUser(connectionId);

            // Get the other user
            var otherUser = GameManager.GetOtherUser(activeGame.GameId, connectionId);

            // Check if the current user already has a piece on this position
            if (!currentUser.HasPieceOnPosition(newPosition))
            {
                // Check if there is a piece of the other user on this position
                if (otherUser.HasPieceOnPosition(newPosition))
                {
                    // Team white can only go down and down diagonal
                    if (clientSide == ClientSide.White)
                    {
                        if ((positionModel.RowOldPosition > positionModel.RowNewPosition) &&
                            ((int) positionModel.ColumnOldPosition != (int) positionModel.ColumnNewPosition))
                        {
                            moveModel.IsValidMove = true;
                        }
                    }
                    else
                    {
                        // Team black can only go up
                        if ((positionModel.RowOldPosition < positionModel.RowNewPosition) &&
                            ((int) positionModel.ColumnOldPosition != (int) positionModel.ColumnNewPosition))
                        {
                            moveModel.IsValidMove = true;
                        }
                    }

                    // If it's a valid move then remove the opponents chess piece
                    if (moveModel.IsValidMove)
                    {
                        // Remove chess piece from other user
                        otherUser.RemovePieceByPosition(newPosition);

                        // Update return model
                        moveModel.DeletePositionId = newPosition;
                    }
                }
                else
                {
                    // Team white can only go down
                    if (clientSide == ClientSide.White)
                    {
                        if (positionModel.RowOldPosition > positionModel.RowNewPosition &&
                            ((int) positionModel.ColumnOldPosition == (int) positionModel.ColumnNewPosition))
                        {
                            moveModel.IsValidMove = true;
                        }
                    }
                    else
                    {
                        // Team black can only go up
                        if (positionModel.RowOldPosition < positionModel.RowNewPosition &&
                            ((int) positionModel.ColumnOldPosition == (int) positionModel.ColumnNewPosition))
                        {
                            moveModel.IsValidMove = true;
                        }
                    }
                }
            }
                // The current user already has a piece on this position
            else
            {
                moveModel.IsValidMove = false;
            }

            // Update position in the users pieces list
            if (moveModel.IsValidMove)
            {
                currentUser.UpdatePiecePosition(pieceId, newPosition);
            }

            // Unvalid move
            return moveModel;
        }

        #endregion

        #region Toren

        private static MoveModel TorenValidMove(string oldPosition, string newPosition,
            string connectionId, string pieceId)
        {
            // Get positions
            var positionsModel = GetPositions(oldPosition, newPosition);

            // Get return movemodel
            var moveModel = new MoveModel();

            // Get current game
            var activeGame = GameManager.GetGame(connectionId);

            // Get current user
            var currentUser = GameManager.GetUser(connectionId);

            // Get the other user
            var otherUser = GameManager.GetOtherUser(activeGame.GameId, connectionId);

            // Check if the current doesn't already a piece on this position
            if (!currentUser.HasPieceOnPosition(newPosition))
            {
                // We can only go up/down/right/left
                if ((positionsModel.RowNewPosition == positionsModel.RowOldPosition) ||
                    (positionsModel.ColumnNewPosition == positionsModel.ColumnOldPosition))
                {
                    // Check if there is a piece blocking the way from his own team
                    if (TorenValidWay(positionsModel.RowOldPosition, positionsModel.RowNewPosition,
                        positionsModel.ColumnOldPosition, positionsModel.ColumnNewPosition, currentUser))
                    {
                        // Check if there is a piece blocking the way from the opponent
                        if (TorenValidWay(positionsModel.RowOldPosition, positionsModel.RowNewPosition,
                            positionsModel.ColumnOldPosition, positionsModel.ColumnNewPosition, otherUser))
                        {
                            moveModel.IsValidMove = true;

                            // Update position of this chess piece in the pieces list from the current user
                            currentUser.UpdatePiecePosition(pieceId, newPosition);
                        }
                    }
                }
            }

            // Is there a piece on this position?
            if (otherUser.HasPieceOnPosition(newPosition))
            {
                // Remove the piece from this position from the opponent
                otherUser.RemovePieceByPosition(newPosition);

                // Set position of the deleted item in the model
                moveModel.DeletePositionId = newPosition;
            }

            return moveModel;
        }

        private static bool TorenValidWay(int oldRowPosition, int newRowPosition, int oldColumnPosition,
            int newColumnPosition, Client client)
        {
            // Straight down/up
            if (oldColumnPosition == newColumnPosition)
            {
                int lowestRow = oldRowPosition > newRowPosition ? newRowPosition : oldRowPosition;
                int highestRow = oldRowPosition > newRowPosition ? oldRowPosition : newRowPosition;

                // We are going up
                if (oldRowPosition > newRowPosition)
                {
                    lowestRow++;
                }
                    // We are going down
                else
                {
                    lowestRow++;
                    highestRow++;
                }

                for (int counter = lowestRow; counter < highestRow; counter++)
                {
                    var columnPositionLetter =
                        Enum.Parse(typeof (PositionLetter), counter.ToString(CultureInfo.InvariantCulture)).ToString();

                    if (client.HasPieceOnPosition(counter + columnPositionLetter))
                    {
                        return false;
                    }
                }
            }
                // Right/Left
            else
            {
                int lowestColumn = oldColumnPosition > newColumnPosition ? newColumnPosition : oldColumnPosition;
                int highestColumn = oldColumnPosition > newColumnPosition ? oldColumnPosition : newColumnPosition;

                // We are going left
                if (oldColumnPosition > newColumnPosition)
                {
                    lowestColumn++;
                }
                    // We are going right
                else
                {
                    lowestColumn++;
                    highestColumn ++;
                }

                for (int counter = lowestColumn; counter < highestColumn; counter++)
                {
                    var columnPositionLetter =
                        Enum.Parse(typeof(PositionLetter), counter.ToString(CultureInfo.InvariantCulture)).ToString();

                    if (client.HasPieceOnPosition(newRowPosition + columnPositionLetter))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        #endregion

        #region Koningin

        private static bool KoninginValidWay(int oldRowPosition, int newRowPosition, int oldColumnPosition,
        int newColumnPosition, Client client)
        {
            // Straight down/up
            if (oldColumnPosition == newColumnPosition)
            {
                int lowestRow = oldRowPosition > newRowPosition ? newRowPosition : oldRowPosition;
                int highestRow = oldRowPosition > newRowPosition ? oldRowPosition : newRowPosition;

                // We are going up
                if (oldRowPosition > newRowPosition)
                {
                    lowestRow++;
                }
                // We are going down
                else
                {
                    lowestRow++;
                    highestRow++;
                }

                for (int counter = lowestRow; counter < highestRow; counter++)
                {
                    var columnPositionLetter =
                        Enum.Parse(typeof(PositionLetter), counter.ToString(CultureInfo.InvariantCulture)).ToString();

                    if (client.HasPieceOnPosition(counter + columnPositionLetter))
                    {
                        return false;
                    }
                }
            }
            // Right/Left
            else if (oldRowPosition == newRowPosition)
            {
                int lowestColumn = oldColumnPosition > newColumnPosition ? newColumnPosition : oldColumnPosition;
                int highestColumn = oldColumnPosition > newColumnPosition ? oldColumnPosition : newColumnPosition;

                // We are going left
                if (oldColumnPosition > newColumnPosition)
                {
                    lowestColumn++;
                }
                // We are going right
                else
                {
                    lowestColumn++;
                    highestColumn++;
                }

                for (int counter = lowestColumn; counter < highestColumn; counter++)
                {
                    var columnPositionLetter =
                        Enum.Parse(typeof(PositionLetter), counter.ToString(CultureInfo.InvariantCulture)).ToString();

                    if (client.HasPieceOnPosition(newRowPosition + columnPositionLetter))
                    {
                        return false;
                    }
                }
            }
             // Diagonal
            else
            {
                // Get columns
                int lowestColumn = oldColumnPosition > newColumnPosition ? newColumnPosition : oldColumnPosition;
                int highestColumn = oldColumnPosition > newColumnPosition ? oldColumnPosition : newColumnPosition;

                // Get rows
                int lowestRow = oldRowPosition > newRowPosition ? newRowPosition : oldRowPosition;
                int highestRow = oldRowPosition > newRowPosition ? oldRowPosition : newRowPosition;

                int columnCounter = lowestColumn;

                // White team - if we are going left to right down - count from the next step
                if (newRowPosition > oldRowPosition)
                {
                    lowestRow++;
                    columnCounter ++;
                }
                // From right bottom to left top - Dit was eerst omgedraaid
                else if (oldRowPosition < newRowPosition && oldColumnPosition < newColumnPosition)
                {
                    columnCounter++;
                }

                // Start from the next row
                for (int rowCounter = lowestRow; highestRow > rowCounter; rowCounter++, columnCounter++)
                {
                    var columnPositionLetter =
                     Enum.Parse(typeof(PositionLetter), columnCounter.ToString(CultureInfo.InvariantCulture)).ToString();

                    if (client.HasPieceOnPosition(rowCounter + columnPositionLetter))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private static MoveModel KoninginValidMove(string oldPosition, string newPosition,
            string connectionId, string pieceId)
        {
            // Get positions
            var positionsModel = GetPositions(oldPosition, newPosition);

            // Get return model
            var moveModel = new MoveModel();

            // Get game
            var game = GameManager.GetGame(connectionId);

            // Get current user
            var currentUser = GameManager.GetUser(connectionId);

            // Get opponent
            var opponent = GameManager.GetOtherUser(game.GameId, connectionId);

            // Check if the current user doesn't already have a piece on this position
            if (!currentUser.HasPieceOnPosition(newPosition))
            {
                // We can do the same as a toren and loper
                if ((Math.Abs(positionsModel.RowNewPosition - positionsModel.RowOldPosition) ==
                     Math.Abs(positionsModel.ColumnNewPosition - positionsModel.ColumnOldPosition)) ||
                    (positionsModel.RowNewPosition == positionsModel.RowOldPosition) ||
                    (positionsModel.ColumnNewPosition == positionsModel.ColumnOldPosition))
                {
                    // Check if there is a chess piece blocking the way from the current user
                    if (KoninginValidWay(positionsModel.RowOldPosition, positionsModel.RowNewPosition,
                        positionsModel.ColumnOldPosition, positionsModel.ColumnNewPosition, currentUser))
                    {
                          // Check if there is a chess piece blocking the way from the opponent
                        if (KoninginValidWay(positionsModel.RowOldPosition, positionsModel.RowNewPosition,
                            positionsModel.ColumnOldPosition, positionsModel.ColumnNewPosition, opponent))
                        {
                            moveModel.IsValidMove = true;

                            // Update the current piece position
                            currentUser.UpdatePiecePosition(pieceId, newPosition);

                            // Check if the opponent has a piece on this position
                            if (opponent.HasPieceOnPosition(newPosition))
                            {
                                // Remove opponent piece
                                opponent.RemovePieceByPosition(newPosition);

                                // Update return model
                                moveModel.DeletePositionId = newPosition;
                            }
                        }
                    }
                }
            }

            return moveModel;
        }

        #endregion

        #region Koning

        private static MoveModel KoningValidMove(string oldPosition, string newPosition,
            string connectionId, string pieceId)
        {
            // Get positions
            var positionsModel = GetPositions(oldPosition, newPosition);

            // Get return movemodel
            var moveModel = new MoveModel();

            // Get current user
            var currentUser = GameManager.GetUser(connectionId);

            // Check if the current user doesn't already have a piece on this position
            if (!currentUser.HasPieceOnPosition(newPosition))
            {
                // Get the difference between the row and the column. If it's not more than 1 position away then the move is valid
                if (Math.Abs(positionsModel.RowNewPosition - positionsModel.RowOldPosition) == 1 &&
                    Math.Abs(positionsModel.ColumnNewPosition - positionsModel.ColumnOldPosition) == 1)
                {
                    moveModel.IsValidMove = true;

                    // Update current position from the user
                    currentUser.UpdatePiecePosition(pieceId, newPosition);

                    // Get current game
                    var game = GameManager.GetGame(connectionId);

                    // Get opponent
                    var opponent = GameManager.GetOtherUser(game.GameId, connectionId);

                    // Does this opponent has a piece on this position
                    if (opponent.HasPieceOnPosition(newPosition))
                    {
                        // Remove this piece
                        opponent.RemovePieceByPosition(newPosition);

                        // Update return movemodel
                        moveModel.DeletePositionId = newPosition;
                    }
                }
            }

            return moveModel;
        }

        #endregion

        #region Loper

        private static MoveModel LoperValidMove(string oldPosition, string newPosition,
            string connectionId, string pieceId)
        {
            // Get positions
            var positionsModel = GetPositions(oldPosition, newPosition);

            // Get return model
            var moveModel = new MoveModel();

            // Get current game
            var currentGame = GameManager.GetGame(connectionId);

            // Get opponent
            var opponent = GameManager.GetOtherUser(currentGame.GameId, connectionId);

            // Get current user
            var currentUser = GameManager.GetUser(connectionId);

            // Check if the current user doesn't already have a piece on this position
            if (!currentUser.HasPieceOnPosition(newPosition))
            {
                // Check if there's a piece blocking the way from the current user
                if (LoperValidWay(positionsModel.RowOldPosition, positionsModel.RowNewPosition,
                    positionsModel.ColumnOldPosition, positionsModel.ColumnNewPosition, currentUser))
                {
                    // Check if there's a piece blocking the way from the opponent
                    if (LoperValidWay(positionsModel.RowOldPosition, positionsModel.RowNewPosition,
                        positionsModel.ColumnOldPosition, positionsModel.ColumnNewPosition, opponent))
                    {
                        // We can only go diagonal, if we go diagonal, the horizontal and vertical should be equal
                        if (Math.Abs(positionsModel.RowNewPosition - positionsModel.RowOldPosition) ==
                            Math.Abs(positionsModel.ColumnNewPosition - positionsModel.ColumnOldPosition))
                        {
                            moveModel.IsValidMove = true;

                            // Update new position to the current user
                            currentUser.UpdatePiecePosition(pieceId, newPosition);

                            // Check if the opponent has a chess piece on this position
                            if (opponent.HasPieceOnPosition(newPosition))
                            {
                                // Delete this piece from the opponent
                                opponent.RemovePieceByPosition(newPosition);

                                // Update return model
                                moveModel.DeletePositionId = newPosition;
                            }
                        }
                    }
                }
            }
            return moveModel;
        }

        private static bool LoperValidWay(int oldRowPosition, int newRowPosition, int oldColumnPosition,
     int newColumnPosition, Client client)
        {
                // Get columns
                int lowestColumn = oldColumnPosition > newColumnPosition ? newColumnPosition : oldColumnPosition;
                int highestColumn = oldColumnPosition > newColumnPosition ? oldColumnPosition : newColumnPosition;

                // Get rows
                int lowestRow = oldRowPosition > newRowPosition ? newRowPosition : oldRowPosition;
                int highestRow = oldRowPosition > newRowPosition ? oldRowPosition : newRowPosition;

                int columnCounter = lowestColumn;

                // White team - if we are going left to right down - count from the next step
                if (newRowPosition > oldRowPosition)
                {
                    lowestRow++;
                    columnCounter++;
                }
                // From right bottom to left top - Dit was eerst omgedraaid
                else if (oldRowPosition < newRowPosition && oldColumnPosition < newColumnPosition)
                {
                    columnCounter++;
                }

                // Start from the next row
                for (int rowCounter = lowestRow; highestRow > rowCounter; rowCounter++, columnCounter++)
                {
                    var columnPositionLetter =
                     Enum.Parse(typeof(PositionLetter), columnCounter.ToString(CultureInfo.InvariantCulture)).ToString();

                    if (client.HasPieceOnPosition(rowCounter + columnPositionLetter))
                    {
                        return false;
                    }
                }
            return true;
        }

        #endregion

        #region Paard

        private static MoveModel PaardValidMove(string oldPosition, string newPosition,
            string connectionId, string pieceId)
        {
            // Get positions
            var positionsModel = GetPositions(oldPosition, newPosition);

            // Get return movemodel
            var moveModel = new MoveModel();

            // Get current user
            var currentUser = GameManager.GetUser(connectionId);

            int rowDifference = Math.Abs(positionsModel.RowNewPosition - positionsModel.RowOldPosition);

            int columnDifference = Math.Abs(positionsModel.ColumnNewPosition - positionsModel.ColumnOldPosition);

            // Check if the current user doesn't already have a piece on this position
            if (!currentUser.HasPieceOnPosition(newPosition))
            {
                // A horse can only go 1 position up/down and 1 position right/left
                if ((Math.Abs(positionsModel.RowNewPosition - positionsModel.RowOldPosition) == 2 &&
                     Math.Abs(positionsModel.ColumnNewPosition - positionsModel.ColumnOldPosition) == 1) || (Math.Abs(positionsModel.RowNewPosition - positionsModel.RowOldPosition) == 1 &&
                     Math.Abs(positionsModel.ColumnNewPosition - positionsModel.ColumnOldPosition) == 2))
                {
                    moveModel.IsValidMove = true;

                    // Update current position from the user
                    currentUser.UpdatePiecePosition(pieceId, newPosition);

                    // Get current game
                    var game = GameManager.GetGame(connectionId);

                    // Get opponent
                    var opponent = GameManager.GetOtherUser(game.GameId, connectionId);

                    // Does this opponent has a piece on this position
                    if (opponent.HasPieceOnPosition(newPosition))
                    {
                        // Remove this piece
                        opponent.RemovePieceByPosition(newPosition);

                        // Update return movemodel
                        moveModel.DeletePositionId = newPosition;
                    }
                }
            }
            return moveModel;
        }

        #endregion

        #endregion   
    }
}