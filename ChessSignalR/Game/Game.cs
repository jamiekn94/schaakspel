﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using System.Web;
using ChessSignalR.Controllers;
using Microsoft.AspNet.SignalR;

namespace ChessSignalR.Game
{
    public class Game
    {
        public int GameId { get; set; }
        public string CurrentTurnClientId { get; set; }
        public int AmountTurns { get; set; }
        public Collection<Client> Users { get; set; }
        public Timer TmrTimeElapsed { get; set; }

        public Game(int gameId, string connectionId, ClientSide clientSide, string name)
        {
            // Set gameId
            GameId = gameId;

            // Set current turn
            CurrentTurnClientId = connectionId;

            // Initialize users
            Users = new Collection<Client>();

            // Set timer
            TmrTimeElapsed = new Timer();

            // Add connection
           AddUser(connectionId, ClientSide.White, name);
        }

        public void StartTimer()
        {
            // Set interval to 30 seconds
            TmrTimeElapsed.Interval = 30000;

            // Reset
            TmrTimeElapsed.AutoReset = true;

            // Start
            TmrTimeElapsed.Start();

            TmrTimeElapsed.Elapsed += TmrTimeElapsed_Elapsed;
        }

        void TmrTimeElapsed_Elapsed(object sender, ElapsedEventArgs e)
        {
            // Get hub
            var hub = GlobalHost.ConnectionManager.GetHubContext<GameHub>();

            // Create a new instance of the gamehubsender
            var gameHubSender = new GameHubSender(hub);

            // Send elapsed
            gameHubSender.SendTimeElapsed(GameId);
        }

        public void AddUser(string connectionId, ClientSide clientSide, string name)
        {
            // Add new user
            Users.Add(new Client(connectionId, name, clientSide));
        }
    }
}