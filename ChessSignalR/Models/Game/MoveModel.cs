﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChessSignalR.Models.Pieces;

namespace ChessSignalR.Models.Game
{
    public class MoveModel
    {
        public int PieceId { get; set; }
        public string ToPosition { get; set; }
    }
}