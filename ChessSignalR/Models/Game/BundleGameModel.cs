﻿namespace ChessSignalR.Models.Game
{
    public class BundleGameModel
    {
        public MoveModel MoveModel { get; set; }
        /// <summary>
        /// Contains the users available chess pieces id's
        /// </summary>
        public string[] ChessIds { get; set; }
    }
}