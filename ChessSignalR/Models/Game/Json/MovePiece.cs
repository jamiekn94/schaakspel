﻿namespace ChessSignalR.Models.Game.Json
{
    public class MovePiece
    {
        public string OldPosition { get; set; }
        public string NewPosition { get; set; }
        public string PieceId { get; set; }
        public ChessSignalR.Game.Models.MoveModel MoveModel { get; set; }
        public string CurrentTurnConnectionId { get; set; }

        // Property to know which team has the turn
        public int CurrentTurnTeam { get; set; }
    }
}