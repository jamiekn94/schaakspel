﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using ChessSignalR.Game;

namespace ChessSignalR.Models.Game.Json
{
    public class SignupModel
    {
        public int GameId { get; set; }
        public Collection<UserSignupModel> Users { get; set; }
        public string AvailableIds { get; set; }

        private void SetAvailableIds(ClientSide clientSide)
        {
            // Return white id's
            if (clientSide == ClientSide.White)
            {
                var availableIds = new[]
                {
                    "W1", "W2", "W3", "W4", "W5", "W6", "W7", "W8", "W9", "W10", "W11", "W12", "W13", "W14", "W15",
                    "W16"
                };

                // Set available ids
                AvailableIds = string.Join(",", availableIds);
            }
            else
            {
                // Return black id's
                var availableIds =
                      new[]
                    {
                        "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10", "B11", "B12", "B13", "B14", "B15",
                        "B16"
                    };

                // Set available ids
                AvailableIds = string.Join(",", availableIds);
            }
        }

        public SignupModel(ClientSide clientSide, int gameId)
        {
            Users = new Collection<UserSignupModel>();
            GameId = gameId;
            SetAvailableIds(clientSide);
        }

        public void AddUser(UserSignupModel userSignupModel)
        {
            Users.Add(userSignupModel);
        }
    }
}