﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChessSignalR.Models.Game.Json
{
    public class UserSignupModel
    {
        public string ConnectionId { get; set; }
        public string Name { get; set; }
        public int Side { get; set; }
    }
}