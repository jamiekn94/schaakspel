﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChessSignalR.Models.Pieces
{
    public enum ChessPieceModel
    {
        Pion = 0, Paard = 1, Loper = 2, Toren = 3, Koningin = 4, Koning = 5
    }
}