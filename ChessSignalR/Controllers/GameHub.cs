﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using ChessSignalR.Game;
using ChessSignalR.Models.Game.Json;
using ChessSignalR.Repositories;
using ChessSignalR.Repositories.GameRepository;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace ChessSignalR.Controllers
{
    public class GameHub : Hub
    {
        #region Public Requests Methods
        public void Signup(string username)
        {
            // Check if there are any games that still need a new user
            var activeGame = GameManager.GetGameUserWaiting();

            // Bool if we need to inform the other user
            bool informOtherUser = false;

            // Return SignupModel
            SignupModel signupModel;

            // Create a new game
            if (activeGame == null)
            {
                // Add game into the database
                var gameFromDatabase = AddGameIntoDatabase(username);

                // Add game to active games
                GameManager.AddGame(gameFromDatabase.GameId, Context.ConnectionId, username);

                // Set signupmodel
                signupModel = GetSignupModel(gameFromDatabase.GameId, ClientSide.White);
            }
            // Join this game
            else
            {
                // Adds the user into the active game
                AddUserIntoDatabase(activeGame, username);

                // Add user into the active game list
                activeGame.AddUser(Context.ConnectionId, ClientSide.Black, username);

                // Set signupmodel
                signupModel = GetSignupModel(activeGame.GameId, ClientSide.Black);

                // Set bool to inform the user
                informOtherUser = true;

                // Start elapsed timer
                activeGame.StartTimer();
            }

            // Get JSON
            var jsonSignupModel = new JavaScriptSerializer().Serialize(signupModel);

            // Send the signup to the caller
            Clients.Caller.Signup(jsonSignupModel);

            // Do we need to inform the other user about this
            if (informOtherUser)
            {
                // Inform other user about the signup
                var targetUser = GameManager.GetOtherUser(activeGame.GameId, Context.ConnectionId);

                // Send
                Clients.Client(targetUser.ConnectionId).Signup(jsonSignupModel);
            }
        }

        public override Task OnDisconnected()
        {
            // Get game
            var gameInfo = GameManager.GetGame(Context.ConnectionId);

            // Check if the game still exists
            if (gameInfo != null)
            {
                // Get target user
                var targetUser = GameManager.GetOtherUser(gameInfo.GameId,
                    Context.ConnectionId);

                // Check if there is a target user
                if (targetUser != null)
                {
                    // Send disconnect notification to the other user
                    Clients.Client(targetUser.ConnectionId).Disconnect("Uw tegestander heeft het spel verlaten");
                }

                // Delete game
                GameManager.RemoveGame(gameInfo);
            }

            return base.OnDisconnected();
        }

        public void Move(string jsonMove)
        {
            var js = new JavaScriptSerializer();

            // Deserialize json object
            var oldJsonMove = js.Deserialize<MovePiece>(jsonMove);

            // Get other user
            var otherUser = GameManager.GetGame(Context.ConnectionId)
                .Users.First(x => x.ConnectionId != Context.ConnectionId);

            // Get current user
            var currentUser = GameManager.GetUser(Context.ConnectionId);

            // Update class
            oldJsonMove.MoveModel = Pieces.IsValidMove(oldJsonMove.PieceId, oldJsonMove.OldPosition,
                oldJsonMove.NewPosition, Context.ConnectionId, currentUser.Side);

            // If it's a valid move - update the turn
            if (oldJsonMove.MoveModel.IsValidMove)
            {
                // Get active game
                var activeGame = GameManager.GetGame(Context.ConnectionId);

                // Update current turn to the opponent in the game model
                activeGame.CurrentTurnClientId = otherUser.ConnectionId;

                // Update current turn to clientside json
                oldJsonMove.CurrentTurnConnectionId = otherUser.ConnectionId;

                // Update team tur
                oldJsonMove.CurrentTurnTeam = (int) otherUser.Side;

                // Update amount turns
                activeGame.AmountTurns++;
            }
            // Invalid move - it's still the current users turn
            else
            {
                oldJsonMove.CurrentTurnConnectionId = Context.ConnectionId;
            }

            // Serialize class
            var newJsonMove = js.Serialize(oldJsonMove);
            
            // Send move
            Clients.Client(currentUser.ConnectionId).receiveMove(newJsonMove);
            Clients.Client(otherUser.ConnectionId).receiveMove(newJsonMove);
        }

        public void Chat(string message)
        {
            // Get other user
            var targetUser = GameManager.GetOtherUser(GameManager.GetGame(Context.ConnectionId).GameId,
                Context.ConnectionId);

            // Get current user
            var currentUser = GameManager.GetUser(Context.ConnectionId);

            // If there is another user
            if (targetUser != null)
            {
                Clients.Client(targetUser.ConnectionId).chat(currentUser.Name, message);
            }
           
        }
        #endregion

        #region Private Signup Methods

        /// <summary>
        /// Get the signup model to return to the client
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="clientSide"></param>
        /// <returns></returns>
        private SignupModel GetSignupModel(int gameId, ClientSide clientSide)
        {
            // Initialize return signupmodel
            var signupModel = new SignupModel(clientSide, gameId);

            // Get active game
            var activeGame = GameManager.GetGame(gameId);

            // Add users
            foreach (var user in activeGame.Users)
            {
                signupModel.AddUser(new UserSignupModel
                {
                    ConnectionId = user.ConnectionId,
                    Name = user.Name,
                    Side = (int)user.Side
                });
            }

            return signupModel;
        } 

        private Repositories.Game AddGameIntoDatabase(string username)
        {
            // Initialize repositories
            var gameRepository = new GameRepository();
            var gameUserRepository = new GameUserRepository();

            // Add Game
            gameRepository.Add(new Repositories.Game
            {
                AmountTurns = 0,
            });

            // Save query
            gameRepository.SaveChanges();

            // Get game from database
            var gameFromDatabase = gameRepository.GetLastAddedGame();

            // Add user
            gameUserRepository.Add(new GameUser
            {
                ConnectionId = Context.ConnectionId,
                GameId = gameFromDatabase.GameId,
                Name = username
            });

            // Save
            gameUserRepository.SaveChanges();

            // Get last added user
            var userFromDatabase = gameUserRepository.GetLastAddedUser();

            // Update game in the database
            gameFromDatabase.CurrentTurnUser = userFromDatabase.UserId;

            // Save edited game
            gameRepository.SaveChanges();

            // Return added game
            return gameFromDatabase;
        }


        private void AddUserIntoDatabase(Game.Game activeGame, string username)
        {
            // Initialize repositories
            var gameUserRepository = new GameUserRepository();

            // Add user into the database
            gameUserRepository.Add(new GameUser
            {
                ConnectionId = Context.ConnectionId,
                GameId = activeGame.GameId,
                Name = username
            });

            // Save user
            gameUserRepository.SaveChanges();
        }
        #endregion
    }

    public class GameHubSender
    {
        private IHubContext GameHub { get; set; }

        public GameHubSender(IHubContext gameHub)
        {
            GameHub = gameHub;
        }

        #region Private Response Methods

        public void SendTimeElapsed(int gameId)
        {
            // Get active game
            var activeGame = GameManager.GetGame(gameId);

            // Get first user
            var firstUser = activeGame.Users.First();

            // Get second user
            var secondUser = activeGame.Users.Last();

            // Get user which turns is next
            var nextTurnUser = activeGame.Users.First(x => x.ConnectionId != activeGame.CurrentTurnClientId);

            // Update turn connectionId
            activeGame.CurrentTurnClientId = nextTurnUser.ConnectionId;

            // Increase turn
            activeGame.AmountTurns++;

            // Send elapsed to both clients
            GameHub.Clients.Client(firstUser.ConnectionId).sendElapsed(nextTurnUser.ConnectionId, (int) nextTurnUser.Side);
            GameHub.Clients.Client(secondUser.ConnectionId).sendElapsed(nextTurnUser.ConnectionId, (int) nextTurnUser.Side);
        }

        #endregion
    }
}