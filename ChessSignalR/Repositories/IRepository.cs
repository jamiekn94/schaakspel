﻿using System.Linq;

namespace ChessSignalR.Repositories
{
    interface IRepository<TEntityType> where TEntityType : class
    {
        IQueryable<TEntityType> GetAll();

        TEntityType GetById(int id);

        void Remove(TEntityType item);

        void Add(TEntityType item);
    }
}
