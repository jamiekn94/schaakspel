﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;

namespace ChessSignalR.Repositories
{
    public class AbstractRepository<TEntityType> where TEntityType : class
    {
        private readonly DataClasses1DataContext _dataContext;

        public AbstractRepository()
        {
            _dataContext = new DataClasses1DataContext();
        }

        protected  Table<TEntityType> GetTable()
        {
            return _dataContext.GetTable<TEntityType>();
        }

        public void SaveChanges()
        {
            _dataContext.SubmitChanges();
        }
    }
}