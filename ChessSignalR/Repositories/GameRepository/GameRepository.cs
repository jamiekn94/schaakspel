﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChessSignalR.Repositories.GameRepository
{
    public class GameRepository : AbstractRepository<Game> , IRepository<Game>
    {
        public IQueryable<Game> GetAll()
        {
            throw new NotImplementedException();
        }

        public Game GetById(int id)
        {
           return GetTable().First(x => x.GameId == id);
        }

        public void Remove(Game item)
        {
            GetTable().DeleteOnSubmit(item);
        }

        public void Add(Game item)
        {
           GetTable().InsertOnSubmit(item);
        }

        public Game GetLastAddedGame()
        {
            return GetTable().OrderBy(x => x.GameId).First();
        }
    }
}