﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChessSignalR.Repositories.GameRepository
{
    public class GameUserRepository : AbstractRepository<GameUser>, IRepository<GameUser>
    {
        public IQueryable<GameUser> GetAll()
        {
            throw new NotImplementedException();
        }

        public GameUser GetById(int id)
        {
           return GetTable().First(x => x.GameId == id);
        }

        public void Remove(GameUser item)
        {
            GetTable().DeleteOnSubmit(item);
        }

        public void Add(GameUser item)
        {
           GetTable().InsertOnSubmit(item);
        }

        public GameUser GetLastAddedUser()
        {
            return GetTable().OrderByDescending(x => x.UserId).First();
        }
    }
}