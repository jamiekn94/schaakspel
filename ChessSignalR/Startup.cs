﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ChessSignalR.Startup))]
namespace ChessSignalR
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
