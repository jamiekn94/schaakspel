﻿$(document).ready(function () {

    //#region Properties
    var intervalBlackTeam;
    var intervalWhiteTeam;
    var pieceElement = $('#infoGame #MoveModel_PieceId');
    var positionElement = $('#infoGame #MoveModel_ToPosition');
    var selectedPieceElement;
    var oldPosition;
    var arrayAvailableIds;
    var username = "";
    var gameHub = $.connection.gameHub;
    var isFirstPlayer = false;
    var connectionId;
    var currentTurn = "";
    var currentTeamTurn = 0;

    //#endregion

    // #region Check username
    while (username.length < 3) {
        // Create a promt and set the username
        username = prompt('Uw gebruikersnaam:', '');
    }
    // #endregion

    //#region Data client caller
    //#region Signup
    gameHub.client.signup = function(data) {

        // Get json
        var receivedJsonSignup = $.parseJSON(data);

        // We are the first one here, so we are white (SUCH RACISM)
        if (receivedJsonSignup["Users"].length == 1) {

            // Set name
            $('#teamInfo #teamWhite #name').text(receivedJsonSignup["Users"][0].Name);

            // Set available chess pieces
            $('#infoGame #ChessIds').val(receivedJsonSignup["AvailableIds"]);
            arrayAvailableIds = $('#infoGame #ChessIds').val().split(',');

            // Set connectionId
            connectionId = receivedJsonSignup["Users"][0].ConnectionId;

            // Set bool if we are the first player
            isFirstPlayer = true;

            // Set current turn
            currentTurn = connectionId;

        } else {
            $('#teamInfo #teamWhite #name').text(receivedJsonSignup["Users"][0].Name);
            $('#teamInfo #teamBlack #name').text(receivedJsonSignup["Users"][1].Name);

            animateWhiteTeamTurn("0");

            // Set available chess pieces
            if (isFirstPlayer == false) {
                $('#infoGame #ChessIds').val(receivedJsonSignup["AvailableIds"]);
                arrayAvailableIds = $('#infoGame #ChessIds').val().split(',');

                // Set connectionId
                connectionId = receivedJsonSignup["Users"][1].ConnectionId;

                // Set current turn
                currentTurn = connectionId;
            }
        }
    }
    //#endregion

    //#region Move
    gameHub.client.receiveMove = function (data) {

        // Get json
        var receivedJsonMove = $.parseJSON(data);

        // Set move
        if (receivedJsonMove["MoveModel"].IsValidMove) {

            // Update game html
            setMove(receivedJsonMove["OldPosition"], receivedJsonMove["NewPosition"], receivedJsonMove["PieceId"], receivedJsonMove["MoveModel"].DeletePositionId);

            // Update new turn
            currentTurn = receivedJsonMove["CurrentTurnConnectionId"];

            // Log
            console.log("Geldige zet ontvangen");

            // Update team turn
            currentTeamTurn = receivedJsonMove["CurrentTurnTeam"];

            if (currentTeamTurn == "0") {
                animateWhiteTeamTurn(currentTeamTurn);
            } else{
                animateBlackTeamTurn(currentTeamTurn);
            }
        }
            // Not a valid move
        else if (!receivedJsonMove["MoveModel"].IsValidMove && receivedJsonMove["CurrentTurnConnectionId"] == connectionId) {
            alert("Dit is geen geldige zet!");
            // Log
            console.log("Geen geldige move ontvangen");
        } else {
            console.log("Move: Hier klopts iets niet");
        }

        // Log moves
        console.log("RECEIVED MOVEMENT: Oldposition: " + receivedJsonMove["OldPosition"] + " NewPosition: " + receivedJsonMove["NewPosition"] + " PieceId: " + receivedJsonMove["PieceId"]);
    };
    //#endregion

    //#region Disconnect
    gameHub.client.disconnect = function () {

        // Disable everything
        disableUi();
    }
    //#endregion

    //#region Chat
    gameHub.client.chat = function (name, message) {
        $('#chatContainer #chatContent ul').append('<li><b>' + name + ': </b> ' + message);
    }
    //#endregion

    //#region Turn elapsed
    gameHub.client.sendElapsed = function (newTurnConnectionId, teamTurn) {

        // Set new turn
        currentTurn = newTurnConnectionId;

        // Set team turn
        currentTeamTurn = teamTurn;

        // Animate new turn
        if (teamTurn == "0") {
            animateWhiteTeamTurn(currentTeamTurn);
        } else{
            animateBlackTeamTurn(currentTeamTurn);
        }
    }
    //#endregion
    //#endregion

    //#region Animate Turn Methods
    function animateWhiteTeamTurn(turnTeam) {
        console.log("Current team turn: " + currentTeamTurn + "Received team turn: " + turnTeam);

        if (turnTeam == currentTeamTurn) {
                $('#teamInfo #teamWhite').animate({ backgroundColor: '#f4dcbb' }, 1250, function() {
                    $('#teamInfo #teamWhite').animate({ backgroundColor: '#9F753D' }, 1250, function() {
                        animateWhiteTeamTurn("0");
                    });
                });
        } else {

            // Reset color
            $('#teamInfo #teamWhite').css({ 'background-color': '#f4dcbb' });
        }
    }

    function animateBlackTeamTurn(turnTeam) {
        if (turnTeam == currentTeamTurn) {
            
                $('#teamInfo #teamBlack').animate({ backgroundColor: '#23130D' }, 1250, function() {
                    $('#teamInfo #teamBlack').animate({ backgroundColor: '#462619' }, 1250, function() {
                        animateBlackTeamTurn("1");
                    });
                });
        } else {

            // Reset color
            $('#teamInfo #teamBlack').css({ 'background-color': '#462619' });
        }
    }

    //#endregion

    //#region Disable user interface
    function disableUi() {

        // Chat
        $('#chatContainer #chatContent #chatFieldset').attr('disabled', 'disabled');

        // Game
        var gameWidth = $('#chessGameContainer').innerWidth();
        var gameHeight = $('#chessGameContainer').innerHeight();

        $('#chessGameContainer .overlay').width(gameWidth - 30);
        $('#chessGameContainer .overlay').height(gameHeight);
        $('#chessGameContainer .overlay').css('margin-left', '15px');
        $('#chessGameContainer .overlay').show();
    }

    //#region Send chat message
    function sendServerChatMessage() {
        // Get message
        var message = $('#chatContainer #chatContent input[type="text"]').val();

        // Log
        console.log("Username: " + username + " message: " + message);

        // Validate message
        if (message.length > 0) {

            // Send message
            gameHub.server.chat(message);

            // Add message
            $('#chatContainer #chatContent ul').append('<li><b>' + username + ': </b> ' + message);

            // Clean message
            $('#chatContainer #chatContent input[type="text"]').val("");

            // Focus textbox
            $('#chatContainer #chatContent input[type="text"]').focus();

        } else {
            alert('U heeft geen bericht ingevuld.');
        }
    }

    //#endregion
    //#endregion

    //#region Start SignalR
    $.connection.hub.start().done(function () {

        // Signup for a game
        gameHub.server.signup(username);
    });
    //#endregion

    //#region Events

    //#region Click send button
    $('#chatContainer #chatContent input[type="button"]').click(function () {
        sendServerChatMessage();
    });
    //#endregion

    //#region On enter press - send chat
    $(document).keypress(function (e) {
        if (e.which == 13) {

            // Check if the textfield is being focussed
            if ($('#chatContainer #chatContent input[type="text"]').is(':focus')) {
                sendServerChatMessage();
            }

        }
    });
    //#endregion

    //#region Set new position
    $(document).on('click', '.col-md-1-5', function () {

        // Check if it's our turn
        if (currentTurn == connectionId) {
            // Check if we need to unselect the piece that we want to move
            if ($('#infoGame #MoveModel_PieceId').val() == $(this).find('span').attr('Id')) {

                // Unselect this item
                $(this).removeClass('active');

                // Delete pieceId
                pieceElement.val("");
            }
                // Set the new piece that we want to move
            else if ($('#infoGame #MoveModel_PieceId').val().length == 0) {

                // Set selected piece
                selectedPieceElement = $(this).find('span');

                // Log
                console.log("Geselecteerde schaakstuk: " + selectedPieceElement.attr('Id'));

                // Check if we may move this piece
                if ($.inArray(selectedPieceElement.attr('Id'), arrayAvailableIds) != -1) {

                    // Set the old position
                    oldPosition = $(this).attr('Id');

                    // Select this item
                    $(this).addClass('active');

                    // Set id of piece
                    pieceElement.val(selectedPieceElement.attr('Id'));

                } else {
                    alert('U kunt dit element niet verplaatsen.');
                }

                // Set the position we want to move to
            } else {

                // Get the position
                var position = $(this).attr('Id');

                // Select this position
                $(this).addClass('active');

                // Set the position
                positionElement.val(position);

                // Clean piece id
                $('#infoGame #MoveModel_PieceId').val('');

                // Send move to server
                sendMove();
            }
        } else {
            alert("U bent nog niet aan de beurt");
        }
    });
    //#endregion

    //#endregion


    // Sends the move to the server
    function sendMove() {

        // Json object
        var moveJson = {};

        // Fill json object
        moveJson["OldPosition"] = oldPosition,
            moveJson["NewPosition"] = positionElement.val(),
            moveJson["PieceId"] = selectedPieceElement.attr('Id');

        console.log("Old position: " + oldPosition);
        console.log("New position: " + positionElement.val());
        console.log("PieceId: " + selectedPieceElement.attr('Id'));

        // Get json string
        var moveJsonString = JSON.stringify(moveJson);

        // Unselect position
        $('#chessGameContainer .col-md-1-5').removeClass('active');

        // Send to server
        gameHub.server.move(moveJsonString);
    }

    // Set the move in the client
    function setMove(moveOldPosition, movenewPosition, deletePosition) {

        //#region Delete defeated piece
        if (deletePosition != null) {

            // Remove defeated piece
            $('#chessGameContainer #' + deletePosition).find('.span').fadeOut('fast', function () {
                $('#chessGameContainer #' + deletePosition).html('');
            });

        }
        //#endregion

        // Get the piece
        var htmlPiece = $('#' + moveOldPosition).html();

        // Fade the old position
        $('#' + moveOldPosition).find('span').fadeOut('fast', function () {

            // Delete piece from the old position
            $('#' + oldPosition).html('');

            // Set piece to the new position
            $('#' + movenewPosition).html(htmlPiece);

            // Show piece
            $('#' + movenewPosition).find('span').fadeIn();
        });
    }
});
